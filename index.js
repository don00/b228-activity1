console.log("Hello World!");

// Global Object
// Arrays
let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"
];

console.log(students);

/*
	What is the difference between .splice and .slice methods?
		.splice() - removes and element from the given index and adds elements (Mutator method)
		.slice() - copies a portion of the array from the starting index(non mutator method) 
	
	three types of array methods
		Mutator methods
		Non Mutator methods
		Iterator methods
			forEach() 
			map()

*/


// using for each check each items in the array if they are divisible by 5


let arrNum = [ 15, 20, 25, 30, 11 ]

arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5`);
	} else {
		console.log('false');
	}
})


console.log(Math);
console.log(Math.E);
console.log(Math.PI);
console.log(Math.SQRT2);
console.log(Math.SQRT1_2);
console.log(Math.LN2);
console.log(Math.LN10);
console.log(Math.LOG2E);
console.log(Math.LOG10E);


// methods for rounding a number to an Integer

console.log(Math.round(Math.PI)); // rounds to the nearest Integer
console.log(Math.ceil(Math.PI)); // rounds up to the nearest Integer
console.log(Math.floor(Math.PI)); // rounds down to nearest Integer
console.log(Math.trunc(Math.PI)); // rounds only the Integer part(ES6 update)

console.log(Math.sqrt(3.14));

//lowest value in a list of argumens
console.log(Math.min( -1, -2, -4, 0, 1, 2, 3, 4, -3));

//highest value in a list of arguments

console.log(Math.max( -1, -2, -4, 0, 1, 2, 3, 4, -3));


// ====================


/*
Activity


// 	1 How do you create arrays in JS?

let arr = [];


// 	2 How do you access the first character of an array?
let firstChar = arr[0];

//	3 How do you access the last character of an array?
let lastChar = arr[arr.length - 1]

// 4 What array method searches for, and returns the index of a given value in an array? 
indexOf();

// 5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
forEach()

// 6.   What array method creates a new array with elements obtained from a user-defined function?
map()

// 	7.   What array method checks if all its elements satisfy a given condition?
every()

// 	8.   What array method checks if at least one of its elements satisfies a given condition?
some()

// 	9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
false

// 	10.   True or False: array.slice() copies elements from original array and returns these as a new array.
true

*/

// FUNCTION CODING
// Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

const errorMsgs = [
	'error - can only add strings to an array',
	'error - array must NOT be empty',
	'error - all array elements must be strings',
	'error - 2nd argument must be of data type string',
	'error - 2nd argument must be a single character'
]

function verifyString(str){
	return typeof str === 'string';
};

const addToEnd = (str, arr) => {
	if(verifyString(str)){
		arr.push(str);
		return arr;
	}

	return errorMsgs[0];

};


// Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

const addToStart = (str, arr) => {
	if(verifyString(str)){
		arr.unshift(str);
		return arr;
	}

	return errorMsgs[0];

}


// Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.


/*
	Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:

		if array is empty, return "error - array must NOT be empty"
		if at least one array element is NOT a string, return "error - all array elements must be strings"
		if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/

function checkArray(arr){

		if(arr.length === 0){
			return 1;
		}

		if(!arr.every(val => verifyString(val))){
			return 2;
		}

	return -1
}

function checkChar(char, errorCode){

	if(errorCode > 0){
		return errorCode;
	}

	if(!verifyString(char)){
		return 3;
	}

	if(char.length > 1) {
		return 4;
	}

	return -1
}

const checkAllStringsEnding = (arr, char) => {
	let errorCode = checkArray(arr);

	errorCode = checkChar(char, errorCode);

	if(errorCode > 0){
		return errorMsgs[errorCode]
	}
	
	return arr.every(val => val[val.length - 1] === char)

}


// Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

const stringLengthSorter = (arr) => {
	return arr.sort((str1, str2) => str1.length - str2.length || str1.localeCompare(str2));	
}


/*
	Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

		if array is empty, return "error - array must NOT be empty"
		if at least one array element is NOT a string, return "error - all array elements must be strings"
		if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		return the number of elements in the array that start with the character argument, must be case-insensitive

	Use the students array and the character "J" as arguments when testing.
*/

const startsWithCounter	= (arr, char) => {
	let errorCode = checkArray(arr);

	errorCode = checkChar(char, errorCode);

	if(errorCode > 0){
		return errorMsgs[errorCode]
	}

	const filtered = arr.filter(val => val.toLowerCase().startsWith(char.toLowerCase()));
	return filtered.length;
}


/*
	Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:

	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

	Use the students array and the string "jo" as arguments when testing.
*/

const likeFinder = (arr, str) => {
	let errorCode = checkArray(arr);

	errorCode = checkChar(str, errorCode);

	if(errorCode > 0 && errorCode < 4){
		return errorMsgs[errorCode];
	};	

	return arr.filter(val => val.toLowerCase().includes(str.toLowerCase()));

};



// Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

const randomPicker	= (arr) => {
	const index = Math.round(Math.random() * arr.length)
	return arr[index]
}